var fs = require("fs");

var sqlTableName = "areacode";

var globalSqlArr = [];
let parseJsonToSql = () => {
    var codeJson = readCodeFile();
    try {
        codeJson = JSON.parse(codeJson);
    } catch (e) {
        console.log("JSON 转换异常");
    }
    recursive(codeJson, 0);
    whiteToFile(globalSqlArr.join("\n"));
}

function recursive(arr, parent_id) {
    if (!Array.isArray(arr) || arr.length == 0) {
        return;
    }
    let len = arr.length;
    for (let i = 0; i < len; i++) {
        let area = arr[i];
        let code = parseUrlToCode(area.url);
        globalSqlArr.push(buildSql(code, parent_id, area.name));
        recursive(area.child, code);
    }
}

function parseUrlToCode(url) {
    let codeArr = url.split(".")[0].split("/");
    return codeArr.pop();
}

function buildSql(id, parend_id, name) {
    let len = id.toString().length, level;

    switch (len) {
        case 2: level = 1; break;
        case 4: level = 2; break;
        case 6: level = 3; break;
        default: level = 4; break;
    }
    id = completionCodeLength(id);
    parend_id = completionCodeLength(parend_id);
    return `INSERT INTO '${sqlTableName}' VALUES ('${id}', '${parend_id}', '${name}','${level}');`;
}

function completionCodeLength(id) {
    id = id.toString();
    id += "000000000";
    return id.slice(0, 9);
}

function whiteToFile(str) {
    let sqlHeadStr = `DROP TABLE IF EXISTS '${sqlTableName}';

CREATE TABLE '${sqlTableName}' (
'id' int(10) NOT NULL,
'parent_id' int(10) NOT NULL DEFAULT '000000000',
'name' varchar(50) NOT NULL,
'level' varchar(2) NOT NULL,
PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES '${sqlTableName}' WRITE;\n`;

    let sqlFooterStr = `\n\nUNLOCK TABLES;`;

    fs.writeFileSync('./data/areacode.sql', sqlHeadStr + str + sqlFooterStr, { flag: 'w', encoding: 'utf-8', mode: '0666' });
}

function readCodeFile() {
    return fs.readFileSync('./data/areadata.txt', 'utf-8');
}

parseJsonToSql();
